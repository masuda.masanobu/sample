(function() {
  'use strict';

  angular
    .module('gitlabCiSample')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
