/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('gitlabCiSample')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
